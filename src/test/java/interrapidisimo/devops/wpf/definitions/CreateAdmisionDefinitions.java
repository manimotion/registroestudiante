package interrapidisimo.devops.wpf.definitions;

import org.junit.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import interrapidisimo.devops.wpf.steps.CreateAdmisionSteps;

public class CreateAdmisionDefinitions {


	@Given("^The user enter in Registro Estudiante$")
	public void the_user_enter_in_Registro_Estudiante() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		CreateAdmisionSteps.abrirAplicacion();
	}
	
	@Given("^Enter in the module Registrarse$")
	public void enter_in_the_module_Registrarse() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		CreateAdmisionSteps.ingresarModuloAutomatico();
	}
	
	@Test
	@When("^Fill in all the form fields \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"$")
	public void fill_in_all_the_form_fields(String usuario, String nombre, String apellido, String direccion, String telefono, String email) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		CreateAdmisionSteps.ingresarDatos(usuario, nombre,apellido,direccion,telefono,email);
	}
		
	@Then("^the user can register in the platform\\.$")
	public void the_user_can_register_in_the_platform() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	    CreateAdmisionSteps.validarPantalla();
	}

}
